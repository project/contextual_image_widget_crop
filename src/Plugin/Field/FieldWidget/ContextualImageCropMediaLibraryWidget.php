<?php

namespace Drupal\contextual_image_widget_crop\Plugin\Field\FieldWidget;

use Drupal\Component\Serialization\Json;
use Drupal\contextual_image_widget_crop\Form\MediaAjaxForm;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\PluginSettingsInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\ImageStyleInterface;
use Drupal\media_library\Plugin\Field\FieldWidget\MediaLibraryWidget;
use Drupal\responsive_image\Entity\ResponsiveImageStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Widget plugin for contextual image crop Media Library.
 *
 * @FieldWidget(
 *   id = "contextual_image_widget_crop_media_library_widget",
 *   label = @Translation("Contextual Image Widget Crop: Media library"),
 *   description = @Translation("Allows you to select items from the media library, with contextual image cropping."),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   multiple_values = TRUE,
 * )
 */
class ContextualImageCropMediaLibraryWidget extends MediaLibraryWidget {

  protected EntityDisplayRepositoryInterface $entityDisplayRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityDisplayRepository = $container->get('entity_display.repository');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $field_name = $this->fieldDefinition->getName();
    // Build array of field's parents.
    $parents = array_merge($element['#field_parents'], [
      $field_name,
      $delta,
    ]);

    $crop_types = [];
    if ($image_styles = $this->getImageStyles($items->getEntity())) {
      foreach ($image_styles as $image_style) {
        $crop_types[] = $this->getCropType($image_style);
      }
    }

    $crop_types = array_unique(array_filter($crop_types));
    if (empty($crop_types)) {
      return $element;
    }

    $form_state->set('crop_context', $crop_types);

    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
    if (!$referenced_entities = $items->referencedEntities()) {
      return $element;
    }

    foreach ($referenced_entities as $delta => $media_item) {
      if ($media_item->bundle() !== 'image') {
        continue;
      }

      /** @var \Drupal\file\FileInterface $file */
      $file = $media_item->get('field_media_image')->entity;
      $element_parents = array_merge($parents, [$file->id()]);

      $element['selection'][$delta]['rendered_entity'] = [
        '#media' => $media_item,
        [
          '#theme' => 'contextual_image_widget_crop_library_widget_thumbnail',
          '#element_parents' => implode('/', $element_parents),
          '#image' => [
            '#theme' => 'image_style',
            '#style_name' => $image_style->getName(),
            '#uri' => $file->getFileUri(),
          ],
        ],
      ];

      if (isset($element['selection'][$delta]['edit_button'])) {
        // Remove Drupal core edit link, we will user our own.
        unset($element['selection'][$delta]['edit_button']);
      }

      // Render link to open media entity in ajax form.
      $element['selection'][$delta]['edit_ajax_link'] = [
        '#suffix' => ' ' . $media_item->label(),
        '#attributes' => [
          'class' => ['use-ajax', 'edit-media'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode([
            'width' => '90%',
          ]),
        ],
        '#attached' => [
          'library' => 'core/drupal.dialog.ajax',
        ],
      ] + $media_item->toLink('edit / crop', 'edit-form-ajax', [
        'query' => [
          'crop-context' => $crop_types,
          'element-parents' => implode('/', $element_parents),
          'file-uri' => $file->getFileUri(),
          'image-style' => $image_style->id(),
        ],
      ])->toRenderable();
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function openMediaLibrary(array $form, FormStateInterface $form_state) {
    $response = parent::openMediaLibrary($form, $form_state);

    /** @var \Drupal\Core\TempStore\PrivateTempStoreFactory $tempstore */
    $temp_store = \Drupal::service('tempstore.private');
    // Set the crop context.
    $temp_store->get('contextual_image_widget_crop')
      ->set(MediaAjaxForm::CROP_CONTEXT, $form_state->get('crop_context'));

    return $response;
  }

  /**
   * Returns image style(s) for the current field.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\image\ImageStyleInterface[]|array
   *   The image styles.
   */
  protected function getImageStyles(ContentEntityInterface $entity): array {
    $field_image_styles = [];
    $view_modes = $this->entityDisplayRepository->getViewModes($entity->getEntityTypeId());

    foreach (array_keys($view_modes) as $view_mode_id) {
      $view_mode = $this->entityTypeManager
        ->getStorage('entity_view_display')
        ->load($entity->getEntityTypeId() . '.' . $entity->bundle() . '.' . $view_mode_id);
      if (!$view_mode instanceof EntityViewDisplayInterface) {
        continue;
      }

      $plugin = $view_mode->getRenderer($this->fieldDefinition->getName());
      if (!$plugin instanceof PluginSettingsInterface) {
        continue;
      }

      $settings = $plugin->getSettings();
      if (empty($settings['responsive_image_style']) && empty($settings['image_style'])) {
        continue;
      }

      if (!empty($settings['responsive_image_style'])) {
        // Try to determine image style based on responsive image style.
        /** @var \Drupal\responsive_image\Entity\ResponsiveImageStyle $responsive_image_style */
        $responsive_image_style = ResponsiveImageStyle::load($settings['responsive_image_style']);
        foreach ($responsive_image_style->getImageStyleMappings() as $mapping) {
          if (!empty($mapping['image_mapping_type']) && $mapping['image_mapping_type'] !== '_none') {
            switch ($mapping['image_mapping_type']) {
              case 'image_style':
                if (!empty($mapping['image_mapping']) && empty($field_image_styles[$mapping['image_mapping']])) {
                  $field_image_styles[$mapping['image_mapping']] = ImageStyle::load($mapping['image_mapping']);
                }
                break;

              case 'sizes':
                if (!empty($mapping['image_mapping']['sizes_image_styles'])) {
                  foreach ($mapping['image_mapping']['sizes_image_styles'] as $image_style_id) {
                    if (!empty($field_image_styles[$image_style_id])) {
                      continue;
                    }

                    $field_image_styles[$image_style_id] = ImageStyle::load($image_style_id);
                  }
                }
                break;
            }
          }
        }
      }

      if (empty($field_image_styles) && isset($settings['image_style'])) {
        // Try fixed image style as fallback.
        $field_image_styles[$settings['image_style']] = ImageStyle::load($settings['image_style']);
      }
    }

    return $field_image_styles;
  }

  /**
   * Returns crop type for the current field.
   *
   * @param \Drupal\image\ImageStyleInterface $image_style
   *   The image style.
   *
   * @return string|null
   *   The crop type.
   */
  protected function getCropType(ImageStyleInterface $image_style): ?string {
    foreach ($image_style->getEffects() as $effect) {
      if ($effect->getPluginId() !== 'crop_crop') {
        continue;
      }
      if (empty($effect->getConfiguration()['data']['crop_type'])) {
        continue;
      }

      return $effect->getConfiguration()['data']['crop_type'];
    }

    return NULL;
  }

}
