<?php

namespace Drupal\contextual_image_widget_crop\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\media\MediaForm;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Enhances Media form for AJAX purposes.
 */
class MediaAjaxForm extends MediaForm {

  const CROP_CONTEXT = 'crop_context';

  /**
   * The private temp store.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $privateTempStore;

  /**
   * The request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Constructs a MediaAjaxForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $private_temp_store_factory
   *   The private temp store factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface|null $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface|null $time
   *   The time service.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function __construct(EntityRepositoryInterface $entity_repository, PrivateTempStoreFactory $private_temp_store_factory, RequestStack $request_stack, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);

    $this->request = $request_stack->getCurrentRequest();

    $this->privateTempStore = $private_temp_store_factory->get('contextual_image_widget_crop');
    $this->privateTempStore->delete(self::CROP_CONTEXT);

    if ($context = $this->request->query->all('crop-context')) {
      // Store crop context in temp store.
      $this->privateTempStore->set(self::CROP_CONTEXT, $context);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('tempstore.private'),
      $container->get('request_stack'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['#prefix'] = '<div id="media-ajax-form">';
    $form['#suffix'] = '</div>';

    // The status messages that will contain any form errors.
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -60,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);

    $actions['submit']['#ajax'] = [
      'callback' => [$this, 'ajaxCloseDialog'],
    ];

    return $actions;
  }

  /**
   * Callback to close the modal dialog.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function ajaxCloseDialog(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#media-ajax-form', $form));
    }
    else {
      $this->messenger()->deleteAll();

      // Update cropped image.
      $element_parents = $this->request->query->get('element-parents');
      $response->addCommand(new ReplaceCommand('div[data-contextual-image-widget-crop-refresh="' . $element_parents . '"]',
        [
          '#theme' => 'contextual_image_widget_crop_library_widget_thumbnail',
          '#element_parents' => $element_parents,
          '#image' => [
            '#theme' => 'image_style',
            '#style_name' => $this->request->query->get('image-style'),
            '#uri' => $this->request->query->get('file-uri'),
          ],
        ]));

      // Close dialog.
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;
  }

}
