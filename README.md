# Contextual Image Widget Crop

Enhances the Image Widget Crop module when using the Media Library, to take
into account the context of the entity the Image Media entity gets added.

Say you have a node type "blogpost", and it contains a Media entity reference
field for the blog post teaser image. On the "Manage display" page for
blogpost, you have configured the Media image to be displayed through the
image_blog_post image style (which uses for example a 4:3 crop type, from
your list of available crop types).

When adding a Media image to your blog post, you only want to let
editors set the 4:3 crop type, since that will be the one used after
saving the node. Other crop types that might have been configured, are not
relevant in this context, so we do not expose them to the editor, to
simplify their workflow.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/contextual_image_widget_crop).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/contextual_image_widget_crop).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following module:
- [Image Widget Crop](https://www.drupal.org/project/image_widget_crop)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Prerequisites:
    - Have multiple crop types defined at
      `Home > Administration > Configuration > Media > Crop types`
    - Have ImageCropWidget module configured at
      `Home > Administration > Configuration > Media >
      Image Crop Widget settings`
    - Have one or more image styles configured that use cropping, at
      `Home > Administration > Configuration > Media > Image styles`.
- First, configure the form for the entity that has the Media attached:
    - Figure out which entity type(s) / bundle(s) have a Media reference
      field, for which you want to enable this functionality. E.g.
      if this is a Node entity of bundle "blogpost", to go
      `Structure > Content Types > Blog post > "Manage form display"`
    - Find the Media field, and set the widget to
      "Contextual Image Widget Crop: Media library"
- Next, configure the display for the entity that has the Media attached:
    - E.g. if this is a Node entity of bundle "blogpost", to go
      `Structure >  Content Types > Blog post > Manage display"`
    - For the Media field on that page, configure the formatter.
      E.g. "Thumbnail" format with image style "image_blog_post".
- Finally, set our custom widget for the image field on the Media entity.
    - Go to `Home > Administration > Structure > Media types > Image`.
    - For the "Media library" form display, set the widget for field
      "Image" to "Contextual ImageWidget crop". Configure as you would with
      the default ImageWidget crop widget.
- Now if you add a new instance of your entity type, you will open the
  Media Library to select an existing or new Media image. The ImageWidget
  crop tool will only show the relevant crop type (in our case, the one
  used by "image_blog_post", as described above).


## Maintainers

- Sven Decabooter - [svendecabooter](https://www.drupal.org/u/svendecabooter)
